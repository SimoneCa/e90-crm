<?php

$esperto = "'22944', '43718', '44585', '59197', '59198', '70130', '76959', '77570', '85416', '96441'";
$informato = "'9599', '14435', '14504', '16231', '18377', '19758', '19759', '27621', '27622', '68875', '69994', '77569', '79285', '85422', '90748', '92602'";


// 1 trimestrarli senza guida
function senza_guida($wpdb) {
  $query = "SELECT DISTINCT * FROM spedizioni
    WHERE id_prodotto IN ('9599', '14435', '14504', '16231', '18377', '19758', '19759', '27621', '27622', '68875', '69994', '77569', '79285', '85422', '90748', '92602')
    and codice not in (
      SELECT DISTINCT codice FROM spedizioni
      WHERE id_prodotto IN ('9599', '14435', '14504', '16231', '18377', '19758', '19759', '27621', '27622', '68875', '69994', '77569', '79285', '85422', '90748', '92602')
      AND inizio_abb BETWEEN DATE_SUB((DATE_SUB((SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'), INTERVAL 1 MONTH)), INTERVAL 1 DAY) AND (SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni')
    )
    and codice not in (
      SELECT DISTINCT codice FROM spedizioni
      WHERE id_prodotto IN ('9599', '14435', '14504', '16231', '18377', '19758', '19759', '27621', '27622', '68875', '69994', '77569', '79285', '85422', '90748', '92602')
      AND email LIKE 'manualpay%'
      AND prox_pag NOT LIKE '0'
      AND prox_pag <= DATE_ADD(DATE_ADD((SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'), INTERVAL 1 month), interval 1 day)
  )";
  $resultset = $wpdb -> get_results($wpdb -> prepare($query, OBJECT));
  return $resultset;
}

// 2 - annuali con guide
function con_guida($wpdb) {
  $query = "SELECT DISTINCT * FROM spedizioni WHERE id_prodotto IN ('22944', '43718', '44585', '59197', '59198', '70130', '76959', '77570', '85416', '96441')
    AND codice NOT IN (
      SELECT DISTINCT codice FROM spedizioni
      WHERE id_prodotto IN ('22944', '43718', '44585', '59197', '59198', '70130', '76959', '77570', '85416', '96441')
      AND inizio_abb BETWEEN DATE_SUB((DATE_SUB((SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'), INTERVAL 1 MONTH)), INTERVAL 1 DAY)
      AND (SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni')
    )";
  $resultset = $wpdb -> get_results($wpdb -> prepare($query, OBJECT));
  return $resultset;
}

// 3
function benvenuto($wpdb) {
  $query = "SELECT DISTINCT * FROM spedizioni
    WHERE id_prodotto IN ('9599', '14435', '14504', '16231', '18377', '19758', '19759', '27621', '27622', '68875', '69994', '77569', '79285', '85422', '90748', '92602')
    AND inizio_abb BETWEEN DATE_SUB((DATE_SUB((SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'), INTERVAL 1 MONTH)), INTERVAL 1 DAY)
    AND (SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni')";
  $resultset = $wpdb -> get_results($wpdb -> prepare($query, OBJECT));
  return $resultset;
}

// 4 - Esperto con benvenuto
function benvenuto_guida($wpdb){
  $query = "SELECT DISTINCT * FROM spedizioni
    WHERE id_prodotto IN ('22944', '43718', '44585', '59197', '59198', '70130', '76959', '77570', '85416', '96441')
    AND inizio_abb BETWEEN DATE_SUB((DATE_SUB((SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'), INTERVAL 1 MONTH)), INTERVAL 1 DAY)
    AND (SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni')";
  $resultset = $wpdb -> get_results($wpdb -> prepare($query, OBJECT));
  return $resultset;
}

// 5 - manual pay in scadenza
function in_scad($wpdb) {
  $query = "SELECT DISTINCT * FROM spedizioni
    WHERE email LIKE 'manualpay%'
    AND prox_pag NOT LIKE '0'
    AND prox_pag <= DATE_ADD(DATE_ADD((SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'), INTERVAL 1 month), interval 1 day)";
  $resultset = $wpdb -> get_results($wpdb -> prepare($query, OBJECT));
  return $resultset;
}



function creat_date() {
  global $wpdb;
  $query = "SELECT CONVERT_TZ(create_time,'+00:00','+01:00') as ora FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'";
  $resultset = $wpdb -> get_results($wpdb -> prepare($query, OBJECT));
  $var = $resultset[0] -> ora;
  $datetime = ita_month(beauty_datetime($var));
  return $datetime;
}



function display_shippings() {

    global $wpdb;
  ?>

  <!-- DataTables CSS library -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.jqueryui.min.css"/>

  <!-- jQuery $ jQuery UI libraries -->
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.min.js" integrity="sha256-lnH4vnCtlKU2LmD0ZW1dU7ohTTKrcKP50WA9fa350cE=" crossorigin="anonymous"></script>

  <!-- DataTables JS library -->
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.jqueryui.min.js"></script>

  <!-- DataTables Buttons -->
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>

  <!-- DataTables Export table -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>


  <style>
  table td {
    font-size: 12px;
  }
  td {
    text-align: center;
    vertical-align: middle;
  }
  h1, h2 {
    text-align: center;
  }
  </style>


  <header>
    <br/>
    <h1>Liste spedizioni del <?php echo creat_date() ?></h1>
    <br/><br/>


    <!--
    ********************************
    SENZA GUIDA
    -->

    <h2>Senza guida</h2>
  </header>

  <div>
    <table id="senza_guida" class="display">
      <thead>
        <tr>
          <th>Codice</th>
          <th>Nominativo</th>
          <th>Indirizzo</th>
          <th>Citta</th>
          <th>Cap</th>
          <th>Prov.</th>
          <th>Stato</th>
          <th>Note</th>
          <th>Email</th>
          <th>Fine abb.</th>
          <th>Prox rinnovo</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <?php
          $resultset = senza_guida($wpdb);
          foreach ($resultset as $row) {  ?>
            <td><a href="<?php get_site_url() ?>/wp-admin/post.php?post=<?php echo $row -> codice; ?>&action=edit" target="_blank"><?php echo $row -> codice; ?></a></td>
            <td><?php echo $row -> nome, ' ',  $row -> cognome, ' ', $row -> societÃ ; ?></td>
            <td><?php echo $row -> indirizzo1, ' ', $row -> indirizzo2; ?></td>
            <td><?php echo $row -> cittÃ ; ?></td>
            <td><?php echo $row -> cap; ?></td>
            <td><?php echo $row -> provincia; ?></td>
            <td><?php echo $row -> stato; ?></td>
            <td><?php echo $row -> note; ?></td>
            <td><?php echo $row -> email; ?></td>
            <td data-order="<?php echo strtotime($row -> fine_abb);?>"><?php echo ita_month(beauty_date($row -> fine_abb)); ?></td>
            <td data-order="<?php echo strtotime($row -> prox_pag);?>"><?php echo ita_month(beauty_date($row -> prox_pag)); ?></td>
          </tr>
        <?php } ?>
      </tbody>

    </table>
  </div>

  <script>
  $(document).ready(function() {
    var $dTable = $('#senza_guida').DataTable( {
      "order": [[ 0, "asc" ]],
      dom: 'Bfrtip',
      buttons: ['pageLength', 'csv', 'excel'],
    });
  });
  </script>



  <!--
  ********************************
  CON GUIDA
  -->

  <header>
    <br/><br/><br/>
    <h2>Con guida</h2>
  </header>

  <div>
    <table id="con_guida" class="display">
      <thead>
        <tr>
          <th>Codice</th>
          <th>Nominativo</th>
          <th>Indirizzo</th>
          <th>CittÃ </th>
          <th>Cap</th>
          <th>Prov.</th>
          <th>Stato</th>
          <th>Note</th>
          <th>Email</th>
          <th>Fine abb.</th>
          <th>Prox rinnovo</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <?php
          $resultset = con_guida($wpdb);
          foreach ($resultset as $row) {  ?>
            <td><a href="<?php get_site_url() ?>/wp-admin/post.php?post=<?php echo $row -> codice; ?>&action=edit" target="_blank"><?php echo $row -> codice; ?></a></td>
            <td><?php echo $row -> nome, ' ',  $row -> cognome, ' ', $row -> societÃ ; ?></td>
            <td><?php echo $row -> indirizzo1, ' ', $row -> indirizzo2; ?></td>
            <td><?php echo $row -> cittÃ ; ?></td>
            <td><?php echo $row -> cap; ?></td>
            <td><?php echo $row -> provincia; ?></td>
            <td><?php echo $row -> stato; ?></td>
            <td><?php echo $row -> note; ?></td>
            <td><?php echo $row -> email; ?></td>
            <td data-order="<?php echo strtotime($row -> fine_abb);?>"><?php echo ita_month(beauty_date($row -> fine_abb)); ?></td>
            <td data-order="<?php echo strtotime($row -> prox_pag);?>"><?php echo ita_month(beauty_date($row -> prox_pag)); ?></td>
          </tr>
        <?php } ?>
      </tbody>

    </table>
  </div>

  <script>
  $(document).ready(function() {
    var $dTable = $('#con_guida').DataTable( {
      "order": [[ 0, "asc" ]],
      dom: 'Bfrtip',
      buttons: ['pageLength', 'csv', 'excel'],
    });
  });
  </script>




  <!--
  ********************************
  BENVENUTO
  -->

  <header>
    <br/><br/><br/>
    <h2>Benvenuto</h2>
  </header>

  <div>
    <table id="benvenuto" class="display">
      <thead>
        <tr>
          <th>Codice</th>
          <th>Nominativo</th>
          <th>Indirizzo</th>
          <th>CittÃ </th>
          <th>Cap</th>
          <th>Prov.</th>
          <th>Stato</th>
          <th>Note</th>
          <th>Email</th>
          <th>Fine abb.</th>
          <th>Prox rinnovo</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <?php
          $resultset = benvenuto($wpdb);
          foreach ($resultset as $row) {  ?>
            <td><a href="<?php get_site_url() ?>/wp-admin/post.php?post=<?php echo $row -> codice; ?>&action=edit" target="_blank"><?php echo $row -> codice; ?></a></td>
            <td><?php echo $row -> nome, ' ',  $row -> cognome, ' ', $row -> societÃ ; ?></td>
            <td><?php echo $row -> indirizzo1, ' ', $row -> indirizzo2; ?></td>
            <td><?php echo $row -> cittÃ ; ?></td>
            <td><?php echo $row -> cap; ?></td>
            <td><?php echo $row -> provincia; ?></td>
            <td><?php echo $row -> stato; ?></td>
            <td><?php echo $row -> note; ?></td>
            <td><?php echo $row -> email; ?></td>
            <td data-order="<?php echo strtotime($row -> fine_abb);?>"><?php echo ita_month(beauty_date($row -> fine_abb)); ?></td>
            <td data-order="<?php echo strtotime($row -> prox_pag);?>"><?php echo ita_month(beauty_date($row -> prox_pag)); ?></td>
          </tr>
        <?php } ?>
      </tbody>

    </table>
  </div>

  <script>
  $(document).ready(function() {
    var $dTable = $('#benvenuto').DataTable( {
      "order": [[ 0, "asc" ]],
      dom: 'Bfrtip',
      buttons: ['pageLength', 'csv', 'excel'],
    });
  });
  </script>




  <!--
  ********************************
  BENVENUTO ESPERTO
  -->

<header>
  <br/><br/><br/>
  <h2>Benvenuto + guida</h2>
</header>

<div>
  <table id="benvenuto_esp" class="display">
    <thead>
      <tr>
        <th>Codice</th>
        <th>Nominativo</th>
        <th>Indirizzo</th>
        <th>CittÃ </th>
        <th>Cap</th>
        <th>Prov.</th>
        <th>Stato</th>
        <th>Note</th>
        <th>Email</th>
        <th>Fine abb.</th>
        <th>Prox rinnovo</th>
      </tr>
    </thead>

    <tbody>
      <tr>
        <?php
        $resultset = benvenuto_guida($wpdb);
        foreach ($resultset as $row) {  ?>
          <td><a href="<?php get_site_url() ?>/wp-admin/post.php?post=<?php echo $row -> codice; ?>&action=edit" target="_blank"><?php echo $row -> codice; ?></a></td>
          <td><?php echo $row -> nome, ' ',  $row -> cognome, ' ', $row -> societÃ ; ?></td>
          <td><?php echo $row -> indirizzo1, ' ', $row -> indirizzo2; ?></td>
          <td><?php echo $row -> cittÃ ; ?></td>
          <td><?php echo $row -> cap; ?></td>
          <td><?php echo $row -> provincia; ?></td>
          <td><?php echo $row -> stato; ?></td>
          <td><?php echo $row -> note; ?></td>
          <td><?php echo $row -> email; ?></td>
          <td data-order="<?php echo strtotime($row -> fine_abb);?>"><?php echo ita_month(beauty_date($row -> fine_abb)); ?></td>
          <td data-order="<?php echo strtotime($row -> prox_pag);?>"><?php echo ita_month(beauty_date($row -> prox_pag)); ?></td>
        </tr>
      <?php } ?>
    </tbody>

  </table>
</div>

<script>
$(document).ready(function() {
  var $dTable = $('#benvenuto_esp').DataTable( {
    "order": [[ 0, "asc" ]],
    dom: 'Bfrtip',
    buttons: ['pageLength', 'csv', 'excel'],
  });
});
</script>



<!--
********************************
MANUAL PAY IN SCADENZA
-->

  <header>
    <br/><br/><br/>
    <h2>In scadenza (manual pay)</h2>
  </header>

  <div>
    <table id="in_scad" class="display">
      <thead>
        <tr>
          <th>Codice</th>
          <th>Nominativo</th>
          <th>Indirizzo</th>
          <th>CittÃ </th>
          <th>Cap</th>
          <th>Prov.</th>
          <th>Stato</th>
          <th>Note</th>
          <th>Email</th>
          <th>Fine abb.</th>
          <th>Prox rinnovo</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <?php
          $resultset = in_scad($wpdb);
          foreach ($resultset as $row) {  ?>
            <td><a href="<?php get_site_url() ?>/wp-admin/post.php?post=<?php echo $row -> codice; ?>&action=edit" target="_blank"><?php echo $row -> codice; ?></a></td>
            <td><?php echo $row -> nome, ' ',  $row -> cognome, ' ', $row -> societÃ ; ?></td>
            <td><?php echo $row -> indirizzo1, ' ', $row -> indirizzo2; ?></td>
            <td><?php echo $row -> cittÃ ; ?></td>
            <td><?php echo $row -> cap; ?></td>
            <td><?php echo $row -> provincia; ?></td>
            <td><?php echo $row -> stato; ?></td>
            <td><?php echo $row -> note; ?></td>
            <td><?php echo $row -> email; ?></td>
            <td data-order="<?php echo strtotime($row -> fine_abb);?>"><?php echo ita_month(beauty_date($row -> fine_abb)); ?></td>
            <td data-order="<?php echo strtotime($row -> prox_pag);?>"><?php echo ita_month(beauty_date($row -> prox_pag)); ?></td>
          </tr>
        <?php } ?>
      </tbody>

    </table>
  </div>

  <script>
  $(document).ready(function() {
    var $dTable = $('#in_scad').DataTable( {
      "order": [[ 0, "asc" ]],
      dom: 'Bfrtip',
      buttons: ['pageLength', 'csv', 'excel'],
    });
  });
  </script>

  <?php

}

?>
