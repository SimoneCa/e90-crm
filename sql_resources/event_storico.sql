DROP EVENT IF EXISTS storico;

DELIMITER //

CREATE EVENT storico
ON SCHEDULE EVERY 1 MONTH
STARTS '2021-04-18 14:00:00'
#STARTS NOW()

DO
	BEGIN
    
		DECLARE mese DATE;
        DECLARE benvenuto INT DEFAULT 0;
        DECLARE benvenuto_guida INT DEFAULT 0;
        DECLARE guida INT DEFAULT 0;
        DECLARE no_guida INT DEFAULT 0;
        DECLARE manual INT DEFAULT 0;
        DECLARE totale INT DEFAULT 0;
		
        
	# SENZA GUIDA	
	SELECT DISTINCT count(*) FROM spedizioni
    WHERE id_prodotto IN ('9599', '14435', '14504', '16231', '18377', '19758', '19759', '27621', '27622', '68875', '69994', '77569', '79285', '85422', '90748', '92602')
    and codice not in (
      SELECT DISTINCT codice FROM spedizioni
      WHERE id_prodotto IN ('9599', '14435', '14504', '16231', '18377', '19758', '19759', '27621', '27622', '68875', '69994', '77569', '79285', '85422', '90748', '92602')
      AND inizio_abb BETWEEN DATE_SUB((DATE_SUB((SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'), INTERVAL 1 MONTH)), INTERVAL 1 DAY) AND CURDATE()
    )
    and codice not in (
      SELECT DISTINCT codice FROM spedizioni
      WHERE id_prodotto IN ('9599', '14435', '14504', '16231', '18377', '19758', '19759', '27621', '27622', '68875', '69994', '77569', '79285', '85422', '90748', '92602')
      AND email LIKE 'manualpay%'
      AND prox_pag NOT LIKE '0'
      AND prox_pag <= DATE_ADD(DATE_ADD((SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'), INTERVAL 1 month), interval 1 day)
  )
  INTO no_guida;
  
  
# 2 - annuali con guide
SELECT DISTINCT count(*) FROM spedizioni WHERE id_prodotto IN ('22944', '43718', '44585', '59197', '59198', '70130', '76959', '77570', '85416', '96441')
    AND codice NOT IN (
      SELECT DISTINCT codice FROM spedizioni
      WHERE id_prodotto IN ('22944', '43718', '44585', '59197', '59198', '70130', '76959', '77570', '85416', '96441')
      AND inizio_abb BETWEEN DATE_SUB((DATE_SUB((SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'), INTERVAL 1 MONTH)), INTERVAL 1 DAY) AND CURDATE()
    ) INTO guida;

# 3 benvenuto
SELECT DISTINCT count(*) FROM spedizioni
    WHERE id_prodotto IN ('9599', '14435', '14504', '16231', '18377', '19758', '19759', '27621', '27622', '68875', '69994', '77569', '79285', '85422', '90748', '92602')
    AND inizio_abb BETWEEN DATE_SUB((DATE_SUB((SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'), INTERVAL 1 MONTH)), INTERVAL 1 DAY) AND CURDATE()
    into benvenuto;


# 4 - Esperto con benvenuto
SELECT DISTINCT count(*) FROM spedizioni
    WHERE id_prodotto IN ('22944', '43718', '44585', '59197', '59198', '70130', '76959', '77570', '85416', '96441')
    AND inizio_abb BETWEEN DATE_SUB((DATE_SUB((SELECT create_time FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'spedizioni'), INTERVAL 1 MONTH)), INTERVAL 1 DAY) AND CURDATE()
    into benvenuto_guida;


# 5 - manual pay in scadenza
SELECT DISTINCT count(*) FROM spedizioni
    WHERE email LIKE 'manualpay%'
    AND prox_pag NOT LIKE '0'
    AND prox_pag <= DATE_ADD(DATE_ADD(CURDATE(), INTERVAL 1 month), interval 1 day)
    INTO manual;
    
select benvenuto + benvenuto_guida + guida + no_guida + manual into totale;

select curdate() into mese;

insert into storico(mese, benvenuto, benvenuto_guida, guida, no_guida, manual, totale)
values (mese, benvenuto, benvenuto_guida, guida, no_guida, manual, totale);
    
    
	END //

DELIMITER ;