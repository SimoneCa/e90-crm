<?php


function get_subscriptions() {
  global $wpdb;
  $query = "SELECT DISTINCT
    t.cognome,
    t.nome,
    t.email,
    t.tel,
    t.piano,
    p.post_status,
    t.inizio_abb,
    t.prox_pag,
    t.fine_abb,
    i.order_item_name,
    t.id_prodotto,
    t.codice,
    t.totale,
    t.pagamento
    FROM utenti t
    JOIN wp_posts p ON t.codice = p.ID
    JOIN wp_woocommerce_order_items i ON i.order_id = t.codice
    WHERE i.order_item_name NOT LIKE 'Tariffa%' AND i.order_item_name NOT LIKE 'Spedizione%'
    AND i.order_item_name NOT LIKE 'Contributo%' AND i.order_item_name NOT LIKE 'xxx%'
    AND i.order_item_name NOT IN ('informato6', 'wellness30', 'esperto10', 'arci10') ";
  $resultset = $wpdb -> get_results($wpdb -> prepare($query, OBJECT)) or die ('Errore nel recuperare i dati!');
  return $resultset;
}


function display_subscriptions() {

?>

<html>
  <head>
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

    <link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <script src="https://nightly.datatables.net/js/jquery.dataTables.js"></script>

    <!-- Select2 plugin -->
  	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <!-- DataTables Export table -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

    <!-- DataTables Buttons -->
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>

    <link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />



    <meta charset=utf-8 />

    <style>
      .ui-datepicker td .ui-state-default {
        padding: .2em!important;
      }
      #baseDateControl {
        float:left;
      }
      #scelta {
        float:left;
      }
    </style>

    <header>
      <br/>
      <h1>Lista sottoscrizioni Salvagente</h1>
      <h2>Ultimo aggiornamento: <?php print_r(ita_month(show_uptime())); ?></h2>
      <br/>
    </header>


    <div id="baseDateControl"  style='margin-bottom:25px;'>
      <div class="dateControlBlock">
        Data <b>inizio abbonamento</b> tra il <input type="date" placeholder="dd-mm-yyyy" name="start5" id="dateStart5" class="datepicker" value="" size="8" />
        e il <input type="date" name="end5" id="dateEnd5" class="datepicker" value="" size="8"/>
        </br>
        Data <b>prossimo pagamento</b> tra il <input type="date" name="start6" id="dateStart6" class="datepicker" value="" size="8"/>
        e il <input type="date" name="end6" id="dateEnd6" class="datepicker" value="" size="8"/>
        </br>
        Data <b>termine abbonamento</b> tra il <input type="date" name="start7" id="dateStart7" class="datepicker" value="" size="8"/>
        e il <input type="date" name="end7" id="dateEnd7" class="datepicker" value="" size="8"/>
        </br>
      </div>
    </div>

<div/>

<div id="scelta" style='margin-bottom:25px;'>
  Scegli colonne:
  <a class="toggle-vis" data-column="0">Codice</a>
  -
  <a class="toggle-vis" data-column="1">Nome</a>
  -
  <a class="toggle-vis" data-column="2">Cognome</a>
  -
  <a class="toggle-vis" data-column="3">Telefono</a>
  -
  <a class="toggle-vis" data-column="4">E-mail</a>
  -
  <a class="toggle-vis" data-column="5">Tipo abbonamento</a>
  -
  <a class="toggle-vis" data-column="6">Piano</a>
  -
  <a class="toggle-vis" data-column="7">Stato</a>
  -
  <a class="toggle-vis" data-column="8">Totale</a>
  -
  <a class="toggle-vis" data-column="9">Metodo pagamento</a>
  -
  <a class="toggle-vis" data-column="10">Inizio abbonamento</a>
  -
  <a class="toggle-vis" data-column="11">Prossimo pagamento</a>
  -
  <a class="toggle-vis" data-column="12">Termine abbonamento</a>
  -
  <a class="toggle-vis" data-column="13">Cambio status</a>
</div>

  <br/>
  <br/>


  </head>
  <body>
    <div class="container">
      <table id="example" class="display nowrap" width="100%">
        <thead>
          <tr>
            <th>Codice</th>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Telefono</th>
            <th>E-mail</th>
            <th>Tipo abbonamento</th>
            <th>Piano</th>
            <th>Stato</th>
            <th>Totale</th>
            <th>Metodo pagamento</th>
            <th>Inizio abbonamento</th>
            <th>Prossimo pagamento</th>
            <th>Termine abbonamento</th>
            <th>Cambio status</th>
          </tr>
        </thead>

        <tfoot>
          <tr>
            <th>Codice</th>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Telefono</th>
            <th>E-mail</th>
            <th>Tipo abbonamento</th>
            <th>Piano</th>
            <th>Stato</th>
            <th>Totale</th>
            <th>Metodo pagamento</th>
            <th>Inizio abbonamento</th>
            <th>Prossimo pagamento</th>
            <th>Termine abbonamento</th>
            <th>Cambio status</th>
          </tr>
        </tfoot>

        <tbody>
          <tr>
            <?php
            $resultset = get_subscriptions();
            foreach ($resultset as $row) {  ?>

              <td><a href="<?php get_site_url() ?>/wp-admin/post.php?post=<?php echo $row -> codice; ?>&action=edit" target="_blank"><?php echo $row -> codice; ?></a></td>
              <td><?php echo $row -> nome; ?></td>
              <td><?php echo $row -> cognome; ?></td>
              <td><?php echo $row -> tel; ?></td>
              <td><?php echo $row -> email; ?></td>

              <td><?php if (group($row -> id_prodotto) === '') {
                echo $row -> order_item_name;
              }
              else { echo group($row -> id_prodotto); } ?></td>

              <td><?php echo beautystr($row -> piano); ?></td>
              <td><?php echo beautystr($row -> post_status); ?></td>
              <td><?php echo 'E. ', $row -> totale; ?></td>

              <td><?php if (($row -> pagamento) == null || ($row -> pagamento) == '') {
                echo 'manual pay';
              }
              else { echo $row -> pagamento; } ?>
              </td>

              <td data-order="<?php echo strtotime($row -> inizio_abb);?>"><?php echo beauty_date($row -> inizio_abb); ?></td>
              <td data-order="<?php echo strtotime($row -> prox_pag);?>"><?php echo beauty_date($row -> prox_pag); ?></td>
              <td data-order="<?php echo strtotime($row -> fine_abb);?>"><?php echo beauty_date($row -> fine_abb); ?></td>
              <td><?php echo cambio_status($row -> fine_abb); ?> gg.</td>

            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
</html>

<script>


  // The plugin function for adding a new filtering routine
  $.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
      var dateStart5 = new Date($("#dateStart5").val());
      var dateEnd5 = new Date($("#dateEnd5").val());
      var evalDate5 = new Date(aData[10]);
      if (dateStart5 != null && dateEnd5 != null && evalDate5 != null) {
        if ($("#dateStart5").val() == "" && $("#dateEnd5").val() == "") { return true; }
        if ($("#dateStart5").val() == "" && evalDate5 <= dateEnd5) { return true; }
        if ($("#dateEnd5").val() == "" && evalDate5 >= dateStart5) { return true; }
        if (evalDate5 <= dateEnd5 && evalDate5 >= dateStart5) { return true; }
        return false;
      }
      return false;
    }
  );


  $.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
      var dateStart6 = new Date($("#dateStart6").val());
      var dateEnd6 = new Date($("#dateEnd6").val());
      var evalDate6 = new Date(aData[11]);
      if (dateStart6 != null && dateEnd6 != null && evalDate6 != null) {
        if ($("#dateStart6").val() == "" && $("#dateEnd6").val() == "") { return true; }
        if ($("#dateStart6").val() == "" && evalDate6 <= dateEnd6) { return true; }
        if ($("#dateEnd6").val() == "" && evalDate6 >= dateStart6) { return true; }
        if (evalDate6 <= dateEnd6 && evalDate6 >= dateStart6) { return true; }
        return false;
      }
      return false;
    }
  );

  $.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
      var dateStart7 = new Date($("#dateStart7").val());
      var dateEnd7 = new Date($("#dateEnd7").val());
      var evalDate7 = new Date(aData[12]);
      if (dateStart7 != null && dateEnd7 != null && evalDate7 != null) {
        if ($("#dateStart7").val() == "" && $("#dateEnd7").val() == "") { return true; }
        if ($("#dateStart7").val() == "" && evalDate7 <= dateEnd7) { return true; }
        if ($("#dateEnd7").val() == "" && evalDate7 >= dateStart7) { return true; }
        if (evalDate7 <= dateEnd7 && evalDate7 >= dateStart7) { return true; }
        return false;
      }
      return false;
    }
  );


  $(document).ready( function () {

    var table = $('#example').DataTable({

              "order": [[ 10, "desc" ]],

              dom: 'Bfrtip',
              buttons: [
                'pageLength', 'csv', {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
                },
                {
                  extend: 'excelHtml5',
                  text: 'Excel' //,
                  /*customize: function (xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    $('c[r=E2] t', sheet).text('Piano');
                    $('c[r=F2] t', sheet).text('Stato');
                    $('c[r=K2] t', sheet).text('Tipo abbonamento');
                    $('c[r=M2] t', sheet).text('Totale');
                    $('c[r=N2] t', sheet).text('Metodo pagamento');
                  }*/
                }
              ], // buttons

              initComplete: function () {

              count = 0;
              this.api().columns([5,6,7,13]).every( function () {
                  var title = this.header();
                  //replace spaces with dashes
                  title = $(title).html().replace(/[\W]/g, '-');
                  var column = this;
                  var select = $('<select id="' + title + '" class="select2" ></select>')
                       .appendTo( $(column.footer()).empty() )
                      .on( 'change', function () {
                        //Get the "text" property from each selected data
                        //regex escape the value and store in array
                        var data = $.map( $(this).select2('data'), function( value, key ) {
                          return value.text ? '^' + $.fn.dataTable.util.escapeRegex(value.text) + '$' : null;
                                   });

                        //if no data selected use ""
                        if (data.length === 0) {
                          data = [""];
                        }

                        //join array into string with regex or (|)
                        var val = data.join('|');

                        //search for the option(s) selected
                        column
                              .search( val ? val : '', true, false )
                              .draw();
                      } );

                  column.data().unique().sort().each( function ( d, j ) {
                      select.append( '<option value="'+d+'">'+d+'</option>' );
                  } );

                //use column title as selector and placeholder
                $('#' + title).select2({
                  multiple: true,
                  closeOnSelect: false,
                  //placeholder: "Select a " + title
                  placeholder: title
                });

                //initially clear select otherwise first option is selected
                $('.select2').val(null).trigger('change');
              } );

              //Event listeners for date pickere
              $("#dateStart7, #dateEnd7, #dateStart6, #dateEnd6, #dateStart5, #dateEnd5").keyup(function() { $table.draw(); });
              $("#dateStart7, #dateEnd7, #dateStart6, #dateEnd6, #dateStart5, #dateEnd5").change(function() { $table.draw(); });

          } // initComplete

    }); //dataTable

      $('a.toggle-vis').on( 'click', function (e) {
      e.preventDefault();
      // Get the column API object
      var column = table.column( $(this).attr('data-column') );
      // Toggle the visibility
      column.visible( ! column.visible() );
      } );

  } ); // document.ready



</script>

<?php
}



function group($id) {

  $abbonamento = '';
  $informato = array(9599, 14435, 14504, 14506, 16231, 18377, 19758, 19759, 26198, 27621, 27622, 32384, 68875, 98876, 69994, 69996, 77569, 85422, 90748, 92602);
  $esperto = array(22943, 22944, 43717, 43718, 44585, 44809, 45151, 45269, 59197, 59198, 65286, 68873, 68874, 69997, 69998, 70130, 76959, 77570, 85416, 92978);
  $digitale = array(11224, 14501, 14505, 19760, 44361);
  $b2b = 96441;
  $cert_1 = 83320;
  $cert_1 = 101020;

  if (in_array($id, $informato)) {
    return $abbonamento = 'Informato';
  }
  elseif (in_array($id, $esperto)) {
    return $abbonamento = 'Esperto';
  }
  elseif (in_array($id, $digitale)) {
    return $abbonamento = 'Digitale';
  }
  elseif ($id == $b2b) {
    return $abbonamento = 'Esperto B2B';
  }
  elseif ($id == $cert_1) {
    return $abbonamento = 'Certificazione mensile 1';
  }
  elseif ($id == $cert_1) {
    return $abbonamento = 'Certificazione mensile 987';
  }
  else {
    return $abbonamento;
  }

} //raggruppa


?>
